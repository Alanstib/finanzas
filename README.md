# INSTRUCCIONES TEST #

Aca dejo los pasos a seguir para poder probar la app en un emulador tanto IOS como Android

### Instalacion ###

* git clone https://Alanstib@bitbucket.org/Alanstib/finanzas.git
* Abrir terminal e ir a la carpeta donde se clono el proyecto
* npm install para instalar las librerias
* cd iOS && pod install && cd .. (esto es para instalar las librerias para iphone)

### Para correr la APP ###

* En IOS -----> npx react-native run-ios (preferentemente probar en ios porque se ve mas linda)
* En android -----> npx react-native run-android

### Login Cliente ###

Usuario: client@getnada.com
Pass: client

### Login Administrador ###

Usuario: admin@getnada.com
Pass: admin

### Estructura de la app ###
La aplicacion consiste en un login donde pueden ingresar tanto clientes como administradores.
Una vez ingresados a la app entrarias a la home (dashboard) donde pueden ver en el menu lateral o en la pantalla principal todos los elementos que pueden visualizar, editar, agregar y/o eliminar.
La aplicacion esta hecha completamente sin ninguna funcionalidad mas que visualizar los componentes, estilos, transiciones, pantallas, etc.
Espero que puedan ver y probar la aplicacion sin ningun conflicto pero para cualquier asistencia no duden en consultarme.