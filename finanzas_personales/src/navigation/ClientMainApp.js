import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import CustomDrawerNavigator from 'components/CustomDrawerNavigator';
import { Icon } from 'native-base';
import Home from 'screens/MainApp/Home';
import Perfil from 'screens/MainApp/Perfil';
import Categorias from 'screens/MainApp/Categorias';
import Gastos from 'screens/MainApp/Gastos';
import Ingresos from 'screens/MainApp/Ingresos';
import Reportes from 'screens/MainApp/Reportes';

const ClientDrawerNavigator = createDrawerNavigator(
    {
        Home: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-home' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Home",
            },
            screen: Home,
        },
        Perfil: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-person' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Mi Perfil",
            },
            screen: Perfil,
        },
        Categorias: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-list' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Categorias",
            },
            screen: Categorias,
        },
        Ingresos: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-trending-up' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Ingresos",
            },
            screen: Ingresos,
        },
        Gastos: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-trending-down' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Gastos",
            },
            screen: Gastos,
        },
        Reportes: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-paper' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Reportes",
            },
            screen: Reportes,
        },
    },
    {
        contentComponent: CustomDrawerNavigator,
    },
);

const AppContainer = createAppContainer(ClientDrawerNavigator);
export default AppContainer;
