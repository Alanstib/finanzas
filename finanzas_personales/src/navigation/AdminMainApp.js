import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import CustomDrawerNavigator from 'components/CustomDrawerNavigator';
import { Icon } from 'native-base';
import Home from 'screens/MainApp/Home';
import Monedas from 'screens/MainApp/Monedas';
import Categorias from 'screens/MainApp/Categorias';
import Clientes from 'screens/MainApp/Clientes';

const AdminDrawerNavigator = createDrawerNavigator(
    {
        Home: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-home' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Home",
            },
            screen: Home,
        },
        Monedas: {
            navigationOptions: {
                drawerIcon: () => <Icon name='logo-usd' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Monedas",
            },
            screen: Monedas,
        },
        Categorias: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-list' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Categorias",
            },
            screen: Categorias,
        },
        Clientes: {
            navigationOptions: {
                drawerIcon: () => <Icon name='ios-people' style={{color: '#003333', fontSize: 25}} />,
                drawerLabel: "Clientes",
            },
            screen: Clientes,
        },
    },
    {
        contentComponent: CustomDrawerNavigator,
    },
);

const AppContainer = createAppContainer(AdminDrawerNavigator);
export default AppContainer;
