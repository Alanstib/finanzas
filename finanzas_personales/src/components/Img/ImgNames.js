const ImgNames = {
    Logo: require('assets/images/Logo.png'),
    Fondo: require('assets/images/Fondo.jpg'),
    Perfil: require('assets/images/Perfil.jpg'),
};

export default ImgNames;