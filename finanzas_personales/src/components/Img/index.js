import React from 'react';
import { Image } from 'react-native';
import Styles from 'assets/css/Styles.js';
import ImgNames from './ImgNames';

const Img = ({ name, style }) => {
    return (
        <Image source={ImgNames[name]} style={Styles[style]} />
    );
};

export default Img;
