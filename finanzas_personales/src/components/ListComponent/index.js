import React, { useContext } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon, ListItem } from 'native-base';
import Styles from 'assets/css/Styles.js';
import { NavigationContext } from 'react-navigation';

const ListComponent = ({icon, title, id, navigate}) => {
    const navigation = useContext(NavigationContext);
    return (
        <ListItem>
            <TouchableOpacity style={Styles.listManagerItem} onPress={()=>{navigation.navigate(navigate, {id})}}>
                <View style={Styles.rowView}>
                    <View style={{width: '15%'}}>
                        <Icon style={{color: '#003333', fontSize: 25}} name={icon} />
                    </View>
                    <View style={{width: '80%', justifyContent: 'center'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#003333'}}>{title}</Text>
                    </View>
                    <View style={{width: '5%', justifyContent: 'center'}}>
                        <Icon style={{color: '#003333', fontSize: 25}} name="arrow-forward" />
                    </View>
                </View>
            </TouchableOpacity>
        </ListItem>
    )
}

export default ListComponent;