import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import Styles from 'assets/css/Styles.js';

const ButtonHeader = ({ navigation, icon }) => {
    return (
        <TouchableOpacity
            style={Styles.botonHeader}
            onPress={() => (icon === 'menu') ? navigation.openDrawer() : navigation.goBack()}>
            <Icon name={icon} style={Styles.headerIcon} />
        </TouchableOpacity>
    );
}

export default ButtonHeader;