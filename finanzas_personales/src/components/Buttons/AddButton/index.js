import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import Styles from 'assets/css/Styles.js';

const AddButton = ({title, onPress, extraStyles}) => {
    return (
        <TouchableOpacity onPress={onPress} style={[Styles.addButton, extraStyles]}>
            <Text style={Styles.addButtonText}>{title}</Text>
            <Icon style={{color: '#fff', fontSize: 25, marginTop: -2, marginLeft: 5}} name="ios-add" />
        </TouchableOpacity>
    );
}

export default AddButton;