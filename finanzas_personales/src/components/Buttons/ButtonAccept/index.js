import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Styles from 'assets/css/Styles.js';

const ButtonAccept = ({title, onPress, extraStyles}) => {
    return (
        <TouchableOpacity onPress={onPress} style={[Styles.buttonAccept, extraStyles]}>
            <Text style={Styles.buttonAcceptText}>{title}</Text>
        </TouchableOpacity>
    );
}

export default ButtonAccept;