import React from "react";
import { View, ScrollView, Text, TouchableOpacity, Alert } from "react-native";
import { DrawerItems } from 'react-navigation-drawer';
import Styles from 'assets/css/Styles.js';
import { Icon } from 'native-base';
import ResetAction from 'utils/ResetAction';
import Image from 'components/Img';

const isLoggingOut = (props) => {
	Alert.alert(
		'Confirmar',
		'Estas seguro que queres cerrar sesión?',
		[
			{ text: 'Si', onPress: () => ResetAction(props.navigation, 'Login') },
			{ text: 'Cancelar' }
		]
	)
}

const CustomDrawerNavigator = props => (
	<View style={{ flex: 1 }}>
	  	<View style={{height: 150}}>
	  		<View style={Styles.header}>
				<View style={Styles.contentDrawerMenu}>
                    <Image name='Logo' style='logoMenu' />
					<View style={Styles.colView}>
                        <Text style={Styles.menuWelcome}>Bienvenido {global.name}</Text>
					</View>
				</View>
	  		</View>
	  	</View>
  		<ScrollView style={{marginTop: 5}}>
		  	<DrawerItems
		  		{...props}
		    	activeBackgroundColor={"#C6C6C6"}
		      	activeTintColor={"#003333"}
				labelStyle={{fontWeight:'400', fontSize: 20, marginLeft: -10, color: '#003333'}}
				itemStyle={{borderBottomWidth: 1, borderBottomColor: '#C6C6C6'}}
		    />
  		</ScrollView>
		<TouchableOpacity 
			onPress={(e) => {
				e.preventDefault();
				isLoggingOut(props);
			}}
			style={Styles.rowView}
		>
			<Icon name='exit' style={Styles.iconLogout} />
			<Text style={Styles.textLogout}>Cerrar Sesion</Text>
		</TouchableOpacity>
  	</View>
);

export default CustomDrawerNavigator;