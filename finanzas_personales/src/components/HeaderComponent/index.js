import React from 'react';
import { Text, View, StatusBar } from 'react-native';
import { Header, Left } from 'native-base';
import ButtonHeader from 'components/Buttons/ButtonHeader';
import Styles from 'assets/css/Styles.js';

const HeaderComponent = ({navigation, title, icon}) => {
    return (
        <View style={{width: '100%'}}>
            <Header style={Styles.header}>
                <Left style={Styles.headerLeft}>
                    {icon != 'NONE' && <ButtonHeader navigation={navigation} icon={icon || 'arrow-back'} title={title} />}
                    <View style={[Styles.headerTextView, (icon == 'NONE') ? { width: '100%' } : { width: '80%' }]}>
                        <Text style={Styles.headerText}>{title}</Text>
                    </View>
                </Left>
            </Header>
            <StatusBar backgroundColor="#003333" barStyle="light-content" />
        </View>
    )
}

export default HeaderComponent;