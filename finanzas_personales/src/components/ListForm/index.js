import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon, ListItem } from 'native-base';
import Styles from 'assets/css/Styles.js';

const ListForm = ({title, date}) => {
    return (
        <ListItem>
            <View style={Styles.rowView}>
                <View style={{width: '10%'}}>
                    <Icon style={{color: '#003333', fontSize: 25}} name="ios-arrow-dropright-circle" />
                </View>
                {date ? (
                    <>
                        <View style={{width: '35%', justifyContent: 'center'}}>
                            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#003333'}}>{title}</Text>
                        </View>
                        <View style={{width: '35%', justifyContent: 'center'}}>
                            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#003333'}}>{date}</Text>
                        </View>
                    </>
                ) : (
                    <View style={{width: '70%', justifyContent: 'center'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#003333'}}>{title}</Text>
                    </View> 
                )}
                <TouchableOpacity style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                    <Icon style={{color: '#003333', fontSize: 25}} name="md-create" />
                </TouchableOpacity>
                <TouchableOpacity style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                    <Icon style={{color: '#003333', fontSize: 25}} name="ios-trash" />
                </TouchableOpacity>
            </View>
        </ListItem>
    )
}

export default ListForm;