import React, { useState } from 'react';
import { Platform, View } from 'react-native';
import { Picker as NativePicker } from 'react-native-picker-dropdown';

function SelectPicker({ options, value, extraStyles }) {
    const [newvalue, setNewvalue] = useState(value);

    if (Platform.OS === 'android') {
        return (
            <View style={{flexDirection: 'column', marginBottom: 16}}>
                <NativePicker
                    selectedValue={newvalue}
                    onValueChange={(itemValue, _) => setNewvalue(itemValue)}
                    textStyle={{ color: '#003333', fontSize: 14 }}
                >
                    <NativePicker.Item
                        label="Sin especificar"
                        value=""
                        color="lightgrey"
                    />
                    {options &&
                        options.map(({ name, value }, index) => (
                            <NativePicker.Item label={name} key={index} value={value} />
                        ))}
                </NativePicker>
            </View>
        );
    }

    return (
        <View style={[{paddingLeft: 2, marginBottom: 16, width: '80%', borderBottomWidth: 1}, extraStyles]}>
            <NativePicker
                selectedValue={newvalue}
                onValueChange={(itemValue, _) => setNewvalue(itemValue)}
                textStyle={{ color: '#003333', fontSize: 14, paddingVertical: 16, alignSelf: 'flex-start' }}
            >
                {options &&
                    options.map(({ name, value }, index) => (
                        <NativePicker.Item label={name} key={index} value={value} />
                    ))}
            </NativePicker>
        </View>
    );
}

export default SelectPicker;
