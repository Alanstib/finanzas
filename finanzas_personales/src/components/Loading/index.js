import React from 'react';
import { ActivityIndicator } from 'react-native';

const Loading = () => {
    return <ActivityIndicator size="large" color="#fff" style={{alignItems: 'center', flex: 1, zIndex: 1}} />
}

export default Loading;