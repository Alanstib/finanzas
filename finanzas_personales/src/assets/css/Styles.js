import { StyleSheet, Platform, StatusBar } from 'react-native';

export default StyleSheet.create({
    rowView: {
        flexDirection: 'row',
    },
    colView: {
        flexDirection: 'column',
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#003333',
        width: '100%',
        justifyContent: 'center',
    },
    headerLeft: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
    },
    headerTextView: {
        alignItems: 'center',
    },
    headerText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 22,
    },
    botonHeader: {
        marginLeft: 10,
    },
    headerIcon: {
        color: '#fff',
        fontSize: 40,
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    },
    container: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        marginTop: 10,
    },
    viewInputsLogin: {
        width: '80%',
        color: '#000',
    },
    viewInputsForm: {
        width: '80%',
        borderColor: '#003333',
        borderWidth: 10,
        paddingHorizontal: 5,
        height: 40,
    },
    viewInputAreaForm: {
        width: '80%',
        borderColor: '#003333',
        borderWidth: 10,
        paddingHorizontal: 5,
    },
    placeholder: {
        fontSize: 16,
        color: '#003333',
    },
    inputTextError: {
        fontSize: 12,
        color: '#FF6961',
        fontWeight: '600',
        opacity: 0.87,
        marginTop: 3,
    },
    buttonAccept: {
        marginTop: 10,
        backgroundColor:'#06C263',
        width: '80%',
        padding: 10,
        alignItems: 'center',
        borderRadius: 10,
    },
    buttonAcceptText: {
        color: '#fff',
        fontWeight: '600',
        fontSize: 16,
    },
    addButton: {
        margin: 10,
        backgroundColor:'#06C263',
        padding: 10,
        alignSelf: 'flex-start',
        borderRadius: 10,
        flexDirection: 'row',
    },
    addButtonText: {
        color: '#fff',
        fontWeight: '600',
        fontSize: 16,
        justifyContent: 'center',
    },
    logoPpal: {
        width: 100,
        height: 100,
        marginTop: 30,
    },
    logoPerfil: {
        width: 150,
        height: 150,
        marginTop: 30,
        borderRadius: 100,
    },
    logoMenu: {
        width: 50,
        height: 50,
    },
    contentDrawerMenu: {
        alignItems:'flex-start',
        width: '100%',
        height: '100%',
        marginLeft: 20,
        paddingTop: 50,
    },
    menuWelcome: {
        color: '#fff',
        fontWeight: '600',
        fontSize: 24,
    },
    iconLogout: {
        color: '#003333',
        fontSize: 20,
        alignSelf: 'center',
        marginBottom: 50,
        marginLeft: 15,
        marginRight: 10,
    },
    textLogout: {
        marginBottom: 50,
        fontSize: 16,
        color: '#003333',
    },
    nameUser: {
        color: '#16122d',
        fontWeight: 'bold',
        fontSize: 28,
        marginBottom: 5
    },
    emailUser: {
        color: '#16122d',
        fontSize: 15,
        fontWeight: 'bold',
    },
    viewInfoUser: {
        marginTop: 25,
        alignItems: 'center',
    },
    editPerfil: {
        fontWeight: 'bold',
        letterSpacing: 0.5,
        color: 'grey',
        marginTop: 50,
        textDecorationLine: 'underline',
    },
    label: {
        color: '#16122d',
        fontSize: 12,
        fontWeight: 'bold',
        marginTop: 20,
    },
    headerDatePicker: {
        width: '100%',
        padding: 16,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#06C263',
    },
    modalContent: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'lightgrey',
    },
    modalBottomContent: {
        backgroundColor: '#fff',
        height: '25%',
    },
    viewBalance: {
        flexDirection: 'row',
        marginTop: '10%',
        marginLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#003333'
    },
    titleBalance: {
        alignSelf: 'flex-start',
        fontSize: 25,
        fontWeight: 'bold',
        color: '#003333',
    },
    valueBalance: {
        position: 'absolute',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#003333',
        right: 10,
    },
    titleTablaReportes: {
        flexDirection: 'row',
        backgroundColor: '#003333',
        paddingVertical: 5,
    },
    tablaReportesContainer: {
        marginVertical: 20,
        justifyContent: 'center',
        borderBottomColor: '#eeeff1',
        borderBottomWidth: 1,
        flexDirection: 'column',
    },
    itemTablaReportes: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#003333',
    },
});