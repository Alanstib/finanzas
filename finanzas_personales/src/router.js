import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './screens/Login';
import AdminMainApp from './navigation/AdminMainApp';
import ClientMainApp from './navigation/ClientMainApp';

const LoginStackNavigator = createStackNavigator(
    {
        Login: Login,
        AdminMainApp: AdminMainApp,
        ClientMainApp: ClientMainApp,
    },
    {
        headerMode: 'none',
    },
);

const AppContainer = createAppContainer(LoginStackNavigator);
export default AppContainer;