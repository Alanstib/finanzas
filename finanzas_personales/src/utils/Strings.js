const Strings = {
    LOGIN: {
        TITLE: 'Finanzas Personales',
        INPUT_USER: 'Email',
        INPUT_PASS: 'Contraseña',
        CONTINUE: 'Continuar',
    },
    GENERAL: {
        TITLE: 'DASHBOARD',
    },
    MONEDAS: {
        TITLE: 'Monedas disponibles',
        BUTTON: 'Nueva moneda',
        ARS: 'ARS',
        USD: 'USD',
    },
    CATEGORIAS: {
        TITLE: 'Categorias disponibles',
        BUTTON: 'Nueva categoria',
        FL: 'Freelance',
        SU: 'Sueldo',
        MER: 'Mercado',
        AWS: 'AWS',
        TR: 'Transporte',
    },
    CLIENTES: {
        TITLE: 'Clientes',
        BUTTON: 'Nuevo cliente',
        NOMBRE1: 'Alan Stiberman',
        DATE1: '24/10/1996',
        NOMBRE2: 'Carla Torres',
        DATE2: '18/06/1995',
        NOMBRE3: 'Mario Gomez',
        DATE3: '05/11/1998',
        NOMBRE4: 'Paula Gimenez',
        DATE4: '10/08/1990',
    },
    INGRESOS: {
        TITLE: 'Ingresos',
        GUARDAR: 'Guardar',
        PLACEHOLDER: {
            TITLE: 'Titulo',
            CATEGORIA: 'Categoria',
            DESCRIPCION: 'Descripcion',
            MONEDA: 'Moneda',
            MONTO: 'Monto',
            DATE: 'Fecha de nacimiento',
        }
    },
    GASTOS: {
        TITLE: 'Gastos',
        GUARDAR: 'Guardar',
        PLACEHOLDER: {
            TITLE: 'Titulo',
            CATEGORIA: 'Categoria',
            DESCRIPCION: 'Descripcion',
            MONEDA: 'Moneda',
            MONTO: 'Monto',
            DATE: 'Fecha de nacimiento',
        }
    },
    REPORTES: {
        TITLE: 'Reportes',
        PROYECCION: 'Proyección de gastos',
        GASTOS_DIARIOS: 'Gastos diarios',
        GASTOS_CATEGORIA: 'Gastos por categoría',
        BALANCE: 'Balance',
        ELEGIR_MES: 'Seleccionar mes',
    },
};

export default Strings;