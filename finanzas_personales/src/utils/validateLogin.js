const validateLogin = (username, pass) => {
    let errorUsername = false;
    let errorPass = false;
    let isValid = true;

    //EMAIL
    var ermail=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;
    if (!username) {
        isValid = false;
        errorUsername = 'Debe ingresar su email.';
    } else if (!(ermail.test(username))) {
        isValid = false;
        errorUsername = 'El Email ingresado no es válido.';
    }

    //PASS
    if (!pass) {
        isValid = false;
        errorPass = 'Debe ingresar una contaseña.';
    }

    if (isValid && ((username == 'admin@getnada.com' && pass == 'admin') || (username == 'client@getnada.com' && pass == 'client'))) {
        isValid = true;
    } else {
        isValid = false;
        errorPass = 'Usuario o contaseña invalido.';
    }
    
    const errors = {
        isValid,
        errorUsername,
        errorPass,
    };
    return errors;
}

export default validateLogin;