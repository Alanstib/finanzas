import { StackActions, NavigationActions } from 'react-navigation';

const ResetAction = (navigation, Screen) => {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: Screen})],
    });
    navigation.dispatch(resetAction);
}

export default ResetAction;