import React from 'react';
import { View } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import Image from 'components/Img';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';
import Form from './Form.js';

const Login = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['LOGIN']['TITLE']} icon='NONE' />
            <Image name='Logo' style='logoPpal' />
            <Form navigation={navigation} />
        </View>
    )
}

export default Login;