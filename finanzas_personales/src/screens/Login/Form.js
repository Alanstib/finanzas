import React, { useState } from 'react';
import { View, Text } from 'react-native';
import { Item, Label, Input, Icon } from 'native-base';
import Styles from 'assets/css/Styles';
import STR from 'utils/Strings';
import validateLogin from 'utils/validateLogin';
import ButtonAccept from 'components/Buttons/ButtonAccept';
import ResetAction from 'utils/ResetAction';

const Form = ({navigation}) => {
    const [username, setUsername] = useState('');
	const [errorUsername, setErrorUsername] = useState(false);
	const [pass, setPass] = useState('');
	const [errorPass, setErrorPass] = useState(false);

    const handleSubmit = () => {
        const validation = validateLogin(username, pass);
        setErrorUsername(validation.errorUsername);
		setErrorPass(validation.errorPass);
		if (validation.isValid) {
            global.name = pass;
            (pass == 'client') ? (
                ResetAction(navigation, 'ClientMainApp')
            ) : (
                ResetAction(navigation, 'AdminMainApp')
            );
        }
    }

    return (
        <View style={Styles.container}>
            <View>
                <Item style={Styles.viewInputsLogin}>
                    <Label style={Styles.placeholder}><Icon name='person' style={{color: '#003333'}} /></Label>
                    <Input
                        placeholder={STR['LOGIN']['INPUT_USER']}
                        style={Styles.placeholder}
                        autoCapitalize="none"
                        onChangeText={username => setUsername(username)}
                        value={username}
                    />
                </Item>
                {errorUsername ? <Text style={Styles.inputTextError}>{errorUsername}</Text> : null}
            </View>
            <View>
                <Item style={Styles.viewInputsLogin}>
                    <Label style={Styles.placeholder}><Icon name='key' style={{color: '#003333'}} /></Label>
                    <Input
                        placeholder={STR['LOGIN']['INPUT_PASS']}
                        secureTextEntry={true}
                        autoCapitalize="none"
                        style={Styles.placeholder}
                        onChangeText={pass => setPass(pass)}
                        value={pass}
                    />
                </Item>
                {errorPass ? <Text style={Styles.inputTextError}>{errorPass}</Text> : null}
            </View>
            <ButtonAccept
                title={STR['LOGIN']['CONTINUE']}
                onPress={(e) => {
                    e.preventDefault();
                    handleSubmit();
                }}
            />
        </View>
    );
}

export default Form;