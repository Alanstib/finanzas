import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import Image from 'components/Img';
import HeaderComponent from 'components/HeaderComponent';
import Styles from 'assets/css/Styles.js';

const Profile = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title='Mi Perfil' icon='arrow-back' />
            <ScrollView>
                <View style={Styles.content}>
                    <View style={{alignItems: 'center'}}>
                        <Image name='Perfil' style='logoPerfil' />
                        <View style={Styles.rowView}>
                            <View style={Styles.viewInfoUser}>
                                <Text style={Styles.nameUser}>Alan Stiberman</Text>
                                <Text style={Styles.emailUser}>24/10/1996</Text>
                            </View>
                        </View>
                        <TouchableOpacity>
                            <Text style={Styles.editPerfil}>EDITAR PERFIL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

export default Profile;