import React from 'react';
import { View, ScrollView } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import ListForm from 'components/ListForm';
import AddButton from 'components/Buttons/AddButton';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';

const Categorias = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['CATEGORIAS']['TITLE']} icon='arrow-back' />
            <View style={{alignSelf: 'flex-start'}}>
                <AddButton title={STR['CATEGORIAS']['BUTTON']} />
            </View>
            <ScrollView>
                <ListForm title={STR['CATEGORIAS']['FL']} />
                <ListForm title={STR['CATEGORIAS']['SU']} />
                <ListForm title={STR['CATEGORIAS']['MER']} />
                <ListForm title={STR['CATEGORIAS']['AWS']} />
                <ListForm title={STR['CATEGORIAS']['TR']} />
            </ScrollView>
        </View>
    )
}

export default Categorias;