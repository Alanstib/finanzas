import React from 'react';
import { View, ScrollView } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import ListForm from 'components/ListForm';
import AddButton from 'components/Buttons/AddButton';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';

const Clientes = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['CLIENTES']['TITLE']} icon='arrow-back' />
            <View style={{alignSelf: 'flex-start'}}>
                <AddButton title={STR['CLIENTES']['BUTTON']} />
            </View>
            <ScrollView>
                <ListForm title={STR['CLIENTES']['NOMBRE1']} date={STR['CLIENTES']['DATE1']} />
                <ListForm title={STR['CLIENTES']['NOMBRE2']} date={STR['CLIENTES']['DATE2']} />
                <ListForm title={STR['CLIENTES']['NOMBRE3']} date={STR['CLIENTES']['DATE3']} />
                <ListForm title={STR['CLIENTES']['NOMBRE4']} date={STR['CLIENTES']['DATE4']} />
            </ScrollView>
        </View>
    )
}

export default Clientes;