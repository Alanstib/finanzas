import React from 'react';
import { Text, ScrollView, View } from "react-native";
import { Label } from 'native-base';
import STR from 'utils/Strings';
import Styles from 'assets/css/Styles';
import SelectPicker from 'components/Picker/SelectPicker';

const TableDiarios = () => {
    return (
        <>
            <View style={{ flexDirection: 'row'}}>
                <Label style={Styles.label}>{STR['REPORTES']['ELEGIR_MES']}</Label>
                <SelectPicker
                    options={[
                        {value: 'ENE', name: 'Enero'},
                        {value: 'FEB', name: 'Febrero'},
                        {value: 'MAR', name: 'Marzo'},
                        {value: 'ABR', name: 'Abril'},
                        {value: 'MAY', name: 'Mayo'},
                        {value: 'JUN', name: 'Junio'},
                        {value: 'JUL', name: 'Julio'},
                        {value: 'AGO', name: 'Agosto'},
                        {value: 'SEP', name: 'Septiembre'},
                        {value: 'OCT', name: 'Octubre'},
                        {value: 'NOV', name: 'Noviembre'},
                        {value: 'DIC', name: 'Diciembre'},
                    ]}
                    value={"ENE"}
                    extraStyles={{width: '30%', alignSelf: 'flex-end', marginLeft: '10%', marginTop: 8, borderBottomWidth: 0}}
                />
            </View>
            <ScrollView style={{width: '100%'}}>
                <View style={Styles.tablaReportesContainer}>
                    <View style={Styles.titleTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>FECHA</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>GASTOS DIARIOS</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>BALANCE</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>01/01/2020</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>-</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>1000 USD</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>02/01/2020</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>1780 ARS</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>1000 USD + 3566 ARS</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>03/01/2020</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>200 USD</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>800 USD + 3000 ARS</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>04/01/2020</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>566 ARS</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>800 USD + 5232 ARS</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

export default TableDiarios;