import React from 'react';
import { Text, ScrollView, View } from "react-native";
import { Label } from 'native-base';
import Styles from 'assets/css/Styles';
import SelectPicker from 'components/Picker/SelectPicker';
import STR from 'utils/Strings';

const TableProyeccion = () => {
    return (
        <>
            <View style={{ flexDirection: 'row'}}>
                <Label style={Styles.label}>{STR['REPORTES']['ELEGIR_MES']}</Label>
                <SelectPicker
                    options={[
                        {value: 'ENE', name: 'Enero'},
                        {value: 'FEB', name: 'Febrero'},
                        {value: 'MAR', name: 'Marzo'},
                        {value: 'ABR', name: 'Abril'},
                        {value: 'MAY', name: 'Mayo'},
                        {value: 'JUN', name: 'Junio'},
                        {value: 'JUL', name: 'Julio'},
                        {value: 'AGO', name: 'Agosto'},
                        {value: 'SEP', name: 'Septiembre'},
                        {value: 'OCT', name: 'Octubre'},
                        {value: 'NOV', name: 'Noviembre'},
                        {value: 'DIC', name: 'Diciembre'},
                    ]}
                    value={"ENE"}
                    extraStyles={{width: '30%', alignSelf: 'flex-end', marginLeft: '10%', marginTop: 8, borderBottomWidth: 0}}
                />
            </View>
            <ScrollView style={{width: '100%'}}>
                <View style={Styles.tablaReportesContainer}>
                    <View style={Styles.titleTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>FECHA</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>TIPO</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>CATEGORIA</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>MONEDA</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>MONTO</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>01/01/2020</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Ingreso</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Freelance</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>USD</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>10000</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>02/01/2020</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Ingreso</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Sueldo</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>ARS</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>5346</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>02/01/2020</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Egreso</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Mercado</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>ARS</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>1780</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>03/01/2020</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Egreso</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>AWS</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>USD</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>200</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>04/01/2020</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Egreso</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Mercado</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>ARS</Text>
                        </View>
                        <View style={{width: '20%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>566</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

export default TableProyeccion;