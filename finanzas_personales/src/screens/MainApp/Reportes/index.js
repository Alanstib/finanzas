import React, { useState } from 'react';
import { View, ScrollView } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';
import ButtonAccept from 'components/Buttons/ButtonAccept';
import TablaReporte from './TablaReporte';

const Reportes = ({navigation}) => {
    const [value, setValue] = useState(1);

    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['REPORTES']['TITLE']} icon='arrow-back' />
            <ButtonAccept
                title={STR['REPORTES']['PROYECCION']}
                onPress={(e) => {
                    e.preventDefault();
                    setValue(1);
                }}
            />
            <ButtonAccept
                title={STR['REPORTES']['GASTOS_DIARIOS']}
                onPress={(e) => {
                    e.preventDefault();
                    setValue(2);
                }}
            />
            <ButtonAccept
                title={STR['REPORTES']['GASTOS_CATEGORIA']}
                onPress={(e) => {
                    e.preventDefault();
                    setValue(3);
                }}
            />
            <ButtonAccept
                title={STR['REPORTES']['BALANCE']}
                onPress={(e) => {
                    e.preventDefault();
                    setValue(4);
                }}
            />
            <TablaReporte value={value} />
        </View>
    )
}

export default Reportes;