import React from 'react';
import { Text, ScrollView, View } from "react-native";
import Styles from 'assets/css/Styles';

const TableBalance = () => {
    return (
        <ScrollView style={{width: '100%'}}>
            <View style={Styles.tablaReportesContainer}>
                <View style={Styles.titleTablaReportes}>
                    <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#fff'}}>MES</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#fff'}}>GASTOS ARS</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#fff'}}>GASTOS USD</Text>
                    </View>
                </View>
                <View style={Styles.itemTablaReportes}>
                    <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>Enero</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>2346</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>200</Text>
                    </View>
                </View>
                <View style={Styles.itemTablaReportes}>
                    <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>Febrero</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>3114</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>30</Text>
                    </View>
                </View>
                <View style={Styles.itemTablaReportes}>
                    <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>Marzo (proyectado)</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>3882</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight: 'bold', color: '#003333'}}>0</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

export default TableBalance;