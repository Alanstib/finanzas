import React from 'react';
import TableDiarios from './TableDiarios';
import TableCategorias from './TableCategorias';
import TableBalance from './TableBalance';
import TableProyeccion from './TableProyeccion';

const TablaReporte = ({value}) => {
    switch (value) {
        case 1: {
            return <TableProyeccion />
        }
        case 2: {
            return <TableDiarios />
        }
        case 3: {
            return <TableCategorias />
        }
        case 4: {
            return <TableBalance />
        }
        default: {
            return null;
        }
    }
}

export default TablaReporte;