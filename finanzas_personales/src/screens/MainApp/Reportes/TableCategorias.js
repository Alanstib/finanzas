import React from 'react';
import { Text, ScrollView, View } from "react-native";
import { Label } from 'native-base';
import STR from 'utils/Strings';
import Styles from 'assets/css/Styles';
import SelectPicker from 'components/Picker/SelectPicker';

const TableCategorias = () => {
    return (
        <>
            <View style={{ flexDirection: 'row'}}>
                <Label style={Styles.label}>{STR['REPORTES']['ELEGIR_MES']}</Label>
                <SelectPicker
                    options={[
                        {value: 'ENE', name: 'Enero'},
                        {value: 'FEB', name: 'Febrero'},
                        {value: 'MAR', name: 'Marzo'},
                        {value: 'ABR', name: 'Abril'},
                        {value: 'MAY', name: 'Mayo'},
                        {value: 'JUN', name: 'Junio'},
                        {value: 'JUL', name: 'Julio'},
                        {value: 'AGO', name: 'Agosto'},
                        {value: 'SEP', name: 'Septiembre'},
                        {value: 'OCT', name: 'Octubre'},
                        {value: 'NOV', name: 'Noviembre'},
                        {value: 'DIC', name: 'Diciembre'},
                    ]}
                    value={"ENE"}
                    extraStyles={{width: '30%', alignSelf: 'flex-end', marginLeft: '10%', marginTop: 8, borderBottomWidth: 0}}
                />
            </View>
            <ScrollView style={{width: '100%'}}>
                <View style={Styles.tablaReportesContainer}>
                    <View style={Styles.titleTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>MES</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>CATEGORIA</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#fff'}}>GASTOS</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Enero</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Mercado</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>2346 ARS</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}></Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>AWS</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>200 USD</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}></Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Freelance</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>2548 ARS</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}></Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Sueldo</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>30 USD</Text>
                        </View>
                    </View>
                    <View style={Styles.itemTablaReportes}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}></Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>Transporte</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontWeight: 'bold', color: '#003333'}}>566 ARS</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

export default TableCategorias;