import React from 'react';
import { View, ScrollView } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import ListForm from 'components/ListForm';
import AddButton from 'components/Buttons/AddButton';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';

const Monedas = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['MONEDAS']['TITLE']} icon='arrow-back' />
            <View style={{alignSelf: 'flex-start'}}>
                <AddButton title={STR['MONEDAS']['BUTTON']} />
            </View>
            <ScrollView>
                <ListForm title={STR['MONEDAS']['ARS']} />
                <ListForm title={STR['MONEDAS']['USD']} />
            </ScrollView>
        </View>
    )
}

export default Monedas;