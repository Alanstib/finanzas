import React from 'react';
import { View, ScrollView } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';
import Form from './Form';

const Ingresos = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['INGRESOS']['TITLE']} icon='arrow-back' />
            <ScrollView style={{width: '100%'}}>
                <Form />
            </ScrollView>
        </View>
    )
}

export default Ingresos;