import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import { Item, Label, Input, Icon } from 'native-base';
import Styles from 'assets/css/Styles';
import STR from 'utils/Strings';
import ButtonAccept from 'components/Buttons/ButtonAccept';
import SelectPicker from 'components/Picker/SelectPicker';

const Form = () => {
    const [titulo, setTitulo] = useState('');
	const [descripcion, setDescripcion] = useState('');
    const [date, setDate] = useState('');
    const [monto, setMonto] = useState('');
    const [crossVisible, setCrossVisible] = useState(false);

    const handleSubmit = () => {
        Alert.alert("Se guardo correctamente el ingreso!");
        setTitulo('');
        setDescripcion('');
        setDate('');
        setMonto('');
    }

    const updateDate = (date) => {
        if (date.length < 11) {
            setDate(date);
            if (date.length == 2 || date.length == 5) {
                setDate(date+'/');
            }
        }
        if (date.length == 10) {
            setCrossVisible(true);
        }
    }

    return (
        <View style={Styles.container}>
            <Text style={{alignSelf: 'flex-start', marginLeft: '10%', fontSize: 25, fontWeight: 'bold'}}>Cargar nuevo Ingreso</Text>
            <View>
                <Label style={Styles.label}>{STR['INGRESOS']['PLACEHOLDER']['TITLE']}</Label>
                <Item style={Styles.viewInputsForm}>
                    <Input
                        placeholder={STR['INGRESOS']['PLACEHOLDER']['TITLE']}
                        style={Styles.placeholder}
                        autoCapitalize="none"
                        onChangeText={titulo => setTitulo(titulo)}
                        value={titulo}
                    />
                </Item>
            </View>
            <View style={{alignSelf: 'flex-start', marginLeft: '10%'}}>
                <Label style={Styles.label}>{STR['INGRESOS']['PLACEHOLDER']['CATEGORIA']}</Label>
            </View>
            <SelectPicker
                options={[
                    {value: 'FL', name: 'Freelance'},
                    {value: 'SU', name: 'Sueldo'},
                    {value: 'MER', name: 'Mercado'},
                    {value: 'AWS', name: 'AWS'},
                    {value: 'TR', name: 'Transporte'},
                ]}
                value={"FL"}
            />
            <View>
                <Label style={Styles.label}>{STR['INGRESOS']['PLACEHOLDER']['DESCRIPCION']}</Label>
                <Item style={Styles.viewInputAreaForm}>
                    <Input
                        placeholder={STR['INGRESOS']['PLACEHOLDER']['DESCRIPCION']}
                        style={[Styles.placeholder, {height: 150, justifyContent: "flex-start"}]}
                        autoCapitalize="none"
                        onChangeText={descripcion => setDescripcion(descripcion)}
                        value={descripcion}
                        multiline = {true}
                    />
                </Item>
            </View>
            <SelectPicker
                options={[
                    {value: 'ARS', name: 'ARS'},
                    {value: 'USD', name: 'USD'},
                ]}
                value={"ARS"}
            />
            <View>
                <Label style={Styles.label}>{STR['INGRESOS']['PLACEHOLDER']['MONTO']}</Label>
                <Item style={Styles.viewInputsForm}>
                    <Input
                        placeholder={STR['INGRESOS']['PLACEHOLDER']['MONTO']}
                        style={Styles.placeholder}
                        autoCapitalize="none"
                        onChangeText={monto => setMonto(monto)}
                        value={monto}
                    />
                </Item>
            </View>
            <View>
                <Label style={Styles.label}>{STR['INGRESOS']['PLACEHOLDER']['DATE']}</Label>
                <Item style={Styles.viewInputsForm}>
                    <Input
                        placeholder={'DD/MM/YYYY'}
                        style={Styles.inputText}
                        onChangeText={date => updateDate(date)}
                        value={date}
                        editable={true}
                    />
                    {crossVisible && <TouchableOpacity onPress={(e) => {e.preventDefault();setDate('');setCrossVisible(false);}}>
                        <Icon name='close' style={{color: 'gray'}} />
                    </TouchableOpacity>}
                </Item>
            </View>
            <ButtonAccept
                title={STR['INGRESOS']['GUARDAR']}
                onPress={(e) => {
                    e.preventDefault();
                    handleSubmit();
                }}
            />
        </View>
    );
}

export default Form;