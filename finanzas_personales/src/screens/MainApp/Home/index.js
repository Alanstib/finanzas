import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import HeaderComponent from 'components/HeaderComponent';
import ListComponent from 'components/ListComponent';
import Styles from 'assets/css/Styles.js';
import STR from 'utils/Strings';

const Home = ({navigation}) => {
    return (
        <View style={Styles.imageBackground}>
            <HeaderComponent navigation={navigation} title={STR['GENERAL']['TITLE']} icon='menu' />
            <ScrollView>
                {global.name == 'admin' ? (
                    <>
                        <ListComponent icon="logo-usd" title="Monedas Disponibles" navigate="Monedas" />
                        <ListComponent icon="ios-list" title="Categorias" navigate="Categorias" />
                        <ListComponent icon="ios-people" title="Clientes" navigate="Clientes" />
                    </>
                ) : (
                    <>
                        <ListComponent icon="ios-person" title="Mi Perfil" navigate="Perfil" />
                        <ListComponent icon="ios-list" title="Categorias" navigate="Categorias" />
                        <ListComponent icon="ios-trending-up" title="Ingresos" navigate="Ingresos" />
                        <ListComponent icon="ios-trending-down" title="Gastos" navigate="Gastos" />
                        <ListComponent icon="ios-paper" title="Reportes" navigate="Reportes" />
                        <View style={Styles.viewBalance}>
                            <Text style={Styles.titleBalance}>Balance</Text>
                            <Text style={Styles.valueBalance}>$1500</Text>
                        </View>
                    </>
                )}
            </ScrollView>
        </View>
    )
}

export default Home;